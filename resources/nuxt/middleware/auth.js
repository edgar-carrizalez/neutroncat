import axios from 'axios'

export default function ({ store, redirect, route }) {
	if(!store.state.session.auth){
		store.commit('session/setPathUrl', route.path)
		axios.get('/app/check-session')
		.then((res) => {
			store.commit('session/init', res.data)
			if(store.state.session.auth){
				redirect(store.state.session.pathUrl)
			} else{
				redirect('/login')
			}
		})
		return redirect('/check-session')
	}
}