// authLanding
import axios from 'axios'

export default function ({ store, redirect }) {
	if(!store.state.session.checked){
		store.commit('session/checkedSession')
		axios.get('/app/check-session')
		.then((res) => {
			store.commit('session/init', res.data)
			redirect('/')
		})
		return redirect('/check-session')
	}
}