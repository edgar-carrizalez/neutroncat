export const state = () => ({
	checked: false,
	auth: null,
	pathUrl: '/dashboard'
})

export const mutations = {
	init(state, session){
		state.auth = session
	},
	setPathUrl(state, url){
		state.pathUrl = url
	},
	checkedSession(state){
		state.checked = true
	},
	close(state){
		state.auth = null
		state.pathUrl = '/dashboard'
	}
}