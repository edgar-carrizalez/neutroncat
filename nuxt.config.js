const laravelNuxt = require("laravel-nuxt");
 
module.exports = laravelNuxt({
	modules: [
		'@nuxtjs/vuetify'
	],
	css: [
		'~/../../node_modules/material-design-icons-iconfont/dist/material-design-icons.css',
		'~/../../node_modules/vuetify/dist/vuetify.min.css'
	],
	plugins: [],
	vuetify: {
		// Vuetify options
		/*theme: {
			primary: "#",
			secondary: "#",
			accent: "#",
			error: "#",
			warning: "#",
			info: "#",
			success: "#",
			verify: "#",
		}*/
	}
});