import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _1e078a90 = () => import('..\\resources\\nuxt\\pages\\register.vue' /* webpackChunkName: "pages_register" */).then(m => m.default || m)
const _25e2cfe8 = () => import('..\\resources\\nuxt\\pages\\check-session.vue' /* webpackChunkName: "pages_check-session" */).then(m => m.default || m)
const _5a572fcf = () => import('..\\resources\\nuxt\\pages\\dashboard.vue' /* webpackChunkName: "pages_dashboard" */).then(m => m.default || m)
const _426f8ba4 = () => import('..\\resources\\nuxt\\pages\\login.vue' /* webpackChunkName: "pages_login" */).then(m => m.default || m)
const _78c7e88d = () => import('..\\resources\\nuxt\\pages\\index.vue' /* webpackChunkName: "pages_index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/register",
			component: _1e078a90,
			name: "register"
		},
		{
			path: "/check-session",
			component: _25e2cfe8,
			name: "check-session"
		},
		{
			path: "/dashboard",
			component: _5a572fcf,
			name: "dashboard"
		},
		{
			path: "/login",
			component: _426f8ba4,
			name: "login"
		},
		{
			path: "/",
			component: _78c7e88d,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
