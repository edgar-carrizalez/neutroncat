<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/app', function () {
	return view('welcome');
});

Route::get('/',
	'\\'.Pallares\LaravelNuxt\Controllers\NuxtController::class
)->where('uri', '.*');

Route::get('/dashboard',
	'\\'.Pallares\LaravelNuxt\Controllers\NuxtController::class
)->where('uri', '.*');

Auth::routes();

Route::post('/app/register', 'Auth\RegisterController@createNuxt')->name('app.register');
Route::post('/app/login', 'Auth\LoginController@loginNuxt')->name('app.login');
Route::get('/app/logout', 'Auth\LoginController@logoutNuxt')->name('app.logout');
Route::get('/app/check-session', 'Auth\LoginController@checkSession')->name('app.check.session');

Route::get('/home', 'HomeController@index')->name('home');
