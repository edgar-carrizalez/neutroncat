<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
use Auth;

class LoginController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){
		$this->middleware('guest')->except(['logout', 'checkSession', 'logoutNuxt']);
	}

	/**
	 * Method for init session in the app.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return json file
	 */
	public function loginNuxt(LoginRequest $request){
		$credentials = $request->only('email', 'password');
		if(Auth::attempt($credentials, $request->remember)){
			$user = array(
				'auth_sesion' => 1,
				'user' => auth()->user()->toArray()
			);
			return response()->json($user);
		} else{
			$errors['errors']['email'] = 'Estas credenciales no coinciden con nuestros registros.';
			return response(json_encode($errors), '422');
		}
	}

	public function checkSession(){
		if(auth()->user()){
			$user = array(
				'auth_sesion' => 1,
				'user' => auth()->user()
			);
			return response()->json($user);
		} else{
			$user = false;
			return response()->json($user);
		}
	}

	public function logoutNuxt(){
		Auth::guard()->logout();
		$user = false;
		return response()->json($user);
	}
}